# README #


### What is this repository for? ###

* This project is a simple logging message that works with PostgreSQL.
* Version: alpha

### Features ###
* Message CRUD
* Account Creation

#### Features in the Pipe ####
* Better UI
* Message detail Popup

### How do I get set up? ###

* Python 2.7.*
* Install a virtualenv wrapper
    * Install Pip
    * run `pip install -r requirements.txt`
* Install Postgresql
    * Update credentials in settings.py
* `source venv/bin/activate` to activate virtualenv
* `./manage.py migrate`
* `./manage.py runserver`
