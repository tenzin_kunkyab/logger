# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse, JsonResponse
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect


from .models import Message
from django.db import IntegrityError
from django.contrib.auth.models import User


def getUserName(userId):
    try:
        userObj = User.objects.get(id=userId)
        return userObj.username
    except User.DoesNotExist:
        return False


def allMessages(userId):
    info = ""
    user_name = getUserName(userId)
    if not user_name:
        info = "No User with ID: %s is found" % userId
        messages = []
    else:
        messages = Message.objects\
            .filter(active=True, recipient=user_name)
    return messages, info


@login_required
def all_logs(request):
    userId = request.user.id
    messages, info = allMessages(userId)
    return render(request, 
                  'log/home.html', 
                  { 'messages': messages, 'info': info })

@login_required
def addLog(request):
    userId = request.user.id
    status = "failure"
    payload = "Incorrect request received"
    if 'recipient' in request.POST and \
        'message' in request.POST and \
        'info' in request.POST:
        recipient = request.POST['recipient']
        message = request.POST['message']
        info = request.POST['info']
        sender = getUserName(userId)

        new_log = Message(userId=userId,
                          message=message,
                          info=info,
                          recipient=recipient,
                          sender=sender)
        new_log.save()
        payload = "Error while creating new Log"
        if new_log.id:
            status = "success"
            payload = "Succesfully added record"
    
    return JsonResponse({ "status": status, "payload": payload })
    

@login_required
def delLog(request):
    """
    this sets the log's active field to False
    """
    status = 'failure'
    if 'msgId' not in request.POST:
        payload = 'Incorrect request sent'
    else:
        msg_id = request.POST['msgId']
        try:
            msgObj = Message.objects.get(pk=msg_id)
            msgObj.active = False
            msgObj.save()
            if msgObj.active == False:
                status = 'success'
            
            payload = 'LogId: %s removed' % msg_id
        
        except Message.DoesNotExist:
            payload = 'LogID: %s doesn\'t exists' % msg_id
        
    return JsonResponse({ 'status': status, 'payload': payload })


def register_user(request):
    status = 'failure'
    payload = ''
    if 'mobile' not in request.POST:
        payload = "Incorrect request sent"
    else:
        mobile = request.POST["mobile"]
        password = request.POST["password"]
        email = request.POST["email"]
        try:
            new_user = User.objects.create_user(mobile, email, password)
            new_user.save()

            payload = "error while creating new user"
            if new_user.id:
                status = "success"
                payload = "Successfully created account, please login now!"
        
        except IntegrityError:
            payload = "Account already registered with details entered. Please Login."
        
        except Exception as e:
            e = e.message if e.message else e
            payload = e
            
    return JsonResponse({ 'status': status, 'payload': payload })
