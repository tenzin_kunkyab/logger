# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-01-16 16:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logApp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='active',
            field=models.BooleanField(default=True),
        ),
    ]
