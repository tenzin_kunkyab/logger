# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class Message(models.Model):
    userId = models.CharField(max_length=30)
    created = models.DateTimeField(auto_now_add=True)
    read = models.DateTimeField(auto_now=True)
    message = models.CharField(max_length=200, blank=True)
    info = models.CharField(max_length=50)
    sender = models.CharField(max_length=20)
    recipient = models.CharField(max_length=30)
    active = models.BooleanField(default=True)

    # class Meta:
    #     app_label = 'msg_data'

    def __unicode__(self):
        return str(self.sender) + str(self.created) \
            + str(self.userId)
