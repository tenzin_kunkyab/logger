from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.all_logs),
    url(r'^add/$', views.addLog),
    url(r'^delete/$', views.delLog),
    url(r'^registration/$', views.register_user)
]
