from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^log/', include('logApp.urls')),
    url(r'^auth/', include('auth.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('django.contrib.auth.urls'))
]# + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)