class mainRouter(object):
    """
    Router to control all database operations on models
    in the auth application
    """
    def db_for_read(self, model, **hints):
        if model.__meta.app_label == 'message_list':
            return 'primary'
        return None
    
    def db_for_write(self, model, **hints):
        if model.__meta.app_label == 'message_list':
            return 'primary'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth app is involved.
        """
        if obj1.__meta.app_label == 'message_list' or \
            obj2.__meta.app_label == 'message_list':
            return 'primary'
        return None
    
    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth app only appears in the 'primary'
        database.
        """
        if app_label == 'message_list':
            return db == 'primary'
        return None